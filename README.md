# PyDeo

PyDeo is a python script that generates simple motion graphics from a text file.
It exports video frames that can then be converted into a video using FFmpeg.

### Note

This is still work in progress: more options, effects and motion will be added in the future.

## Build Dependencies

- Python 3.9.0
- pygame
- Bash (optional)
- FFmpeg (optional)

## Usage

### Step 1

Fill out ```./scripts/words``` file following this basic syntax:

```
w>>h:m:s>>String of characters you wish to display!
```

The string you wish to display will start displaying at the provided timestamp, and will stop displaying once the next timestamp arrives. If no timestamp exists afterwards, the string is simply not displayed and assumed to be the END.

#### Example
```
w>>0:0:0>>This is our first string!
w>>0:0:2>>This is our second string!
w>>0:0:4>>This is our final string.
w>>0:0:7>>END
```

This will display *"This is our first string!"* for 2 seconds (from 0 to 2), then display *"This is our second string!"* for 2 seconds (2 to 4) and finally display *"This is our final string."* for 3 second (4 to 7).

You may use decimal point numbers to define your timestamp.

### Step 2

Run the python script and watch as it randomly selects background colors (from a hard coded list) at each string change!

```bash
python main.py
```
All generated frames will be saved in the ```./frames/``` directory.

### Step 3 (optional)

There are a couple of bash scripts, one will allow you to generate a video file using **FFmpeg** and the other will delete all frames in the frames folder. To generate a video with audio, you can add your audio clip to ```./scripts/audio.wav```
