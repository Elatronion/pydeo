# TODO:
# Clean Up Code
# Add Background Colors
# Select Background Color Randomly
# Load in Background Colors
# Load in Text Colors
# Integrate ffmpeg?
# Integrated Simple Editor (cartoony)

import math
import random
import pygame
import pygame.freetype
import re

import time

def time_format_converter(time_string):
	time_float = 0.0
	
	time_data = re.split('\n|:', time_string)
	print("time_string: ", time_string)
	print("time_data: ", time_data)
	print(time_data[0], "h", time_data[1], "m", time_data[2], "s")
	
	time_float = float(time_data[2]) + float(time_data[1])*60 + float(time_data[0])*3600
	
	return time_float

# Timestamp in seconds
class AnimatedTextElement:
	def __init__(self, timestamp, text):
		self.timestamp = timestamp
		self.text = text

def AddTextElement(text_elements, timestamp, text):
	TextElements.append(AnimatedTextElement(timestamp, text))

def LoadWords(script, text_elements):
    f = open(script, "r")
    input = f.read()
    f.close()
    data = re.split('\n|>>', input)

    CHUNK_STATE = 0

    i = 0
    while i < len(data):
        if data[i] == "w":
            timestamp = time_format_converter(data[i+1])
            text = data[i+2]
            AddTextElement(text_elements, float(timestamp), text)
            print("New Word: [" + str(timestamp) + "] is "+text)
            i+=2
        i+=1


TextElements = []
LoadWords("./scripts/words", TextElements)

if len(TextElements) == 0:
	print("NO WORDS WERE LOADED!")
	exit()

def hex_to_rgb(value):
	h = value.lstrip('#')
	return tuple(int(h[i:i+2], 16) for i in (0, 2, 4))

COLORS_Aurora = [
	hex_to_rgb("#bf616a"),
	hex_to_rgb("#d08770"),
	hex_to_rgb("#ebcb8b"),
	hex_to_rgb("#a3be8c"),
	hex_to_rgb("#b48ead")
]

COLORS_Polar_Night = [
	hex_to_rgb("#2e3440"),
	hex_to_rgb("#3b4252"),
	hex_to_rgb("#434c5e"),
	hex_to_rgb("#4c566a")
]

COLORS_Frost = [
	hex_to_rgb("#8fbcbb"),
	hex_to_rgb("#88c0d0"),
	hex_to_rgb("#81a1c1"),
	hex_to_rgb("#5e81ac")
]

COLORS_Snow_Storm = [
	hex_to_rgb("#d8dee9"),
	hex_to_rgb("#e5e9f0"),
	hex_to_rgb("#eceff4")
]

COLORS_Usergames = [
	hex_to_rgb("#3186D8"),
	hex_to_rgb("#D9733D"),
	hex_to_rgb("#28a428"),
	hex_to_rgb("#cc1b63")
]

# 18
COLORS_PhotoShop = [
	hex_to_rgb("#cd2336"),
	hex_to_rgb("#d15028"),
	hex_to_rgb("#d57523"),
	hex_to_rgb("#dbbf0b"),
	hex_to_rgb("#87ad3f"),
	hex_to_rgb("#389d3e"),
	hex_to_rgb("#078e3d"),
	hex_to_rgb("#07907b"),
	hex_to_rgb("#0aa7cd"),
	hex_to_rgb("#0871a1"),
	hex_to_rgb("#07568e"),
	hex_to_rgb("#2d367f"),
	hex_to_rgb("#542d7e"),
	hex_to_rgb("#7a277f"),
	hex_to_rgb("#ca0a8b"),
	hex_to_rgb("#cc1b63")
]

BG_COLOR_ID = 0
COLORS = COLORS_PhotoShop
cur_bg_color = (0, 0, 0)

TEXT_COLOR_ID = 0
COLORS_TEXT = COLORS_Snow_Storm
cur_text_color = (0, 0, 0)

#COLORS.append((26, 28, 44))
#COLORS.append((93, 39, 93))
#COLORS.append((177, 62, 83))
#COLORS.append((37, 113, 121))
#COLORS.append((51, 60, 87))
#COLORS.append((86, 108, 134))
#COLORS.append((59, 93, 201))
#COLORS.append((56, 183, 100))

FPS = 30
cur_timestamp = 0.0
max_timestamp = TextElements[len(TextElements)-1].timestamp

frame = 0
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

# Initialize pygame
pygame.init()
# Title
pygame.display.set_caption("Python Window")
# Create the screen
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
# Text
font = pygame.font.Font('./fonts/Baloo-Regular.ttf', 32)
#freetype_font = font = pygame.freetype.Font('./fonts/Hack-Bold.ttf')

def SaveFrame():
	global screen
	global frame
	pygame.image.save(screen, "./frames/frame"+str(frame).zfill(4)+".jpeg")

def easeinout(a, b, t):
	diff = b - a;
	return (pow(t, 2)/(pow(t, 2) + pow(1 - t, 2))) * diff + a;

def easeOutExpo(a, b, t):
	diff = b - a;
	if t == 1:
		return b
	else:
		return b - pow(2, -10 * t)

def easeOutBack(x):
	c1 = 1.70158;
	c3 = c1 + 1;
	return 1 + c3 * pow(x - 1, 3) + c1 * pow(x - 1, 2);

#def ScaleSurface(surface, scale):
#	surface_rect = surface.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2))
#	surface = pygame.transform.scale(surface, (surface_rect.width*scale, surface_rect.height*2.0))

def DrawCenterText(str, R, G, B, scale):
	text = font.render(str, True, (R, G, B))
	text_rect = text.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2))
	
	#text = pygame.transform.flip(text, True, False)
	text = pygame.transform.scale(text, (text_rect.width * scale, text_rect.height * scale))
	
	text_rect = text.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2))
	screen.blit(text, text_rect)

def DrawCenterText_tuple(str, color, scale):
	colors = list(color)
	DrawCenterText(str, colors[0], colors[1], colors[2], scale)

def DrawCenterText_hex(str, hex_color, scale):
	DrawCenterText_tuple(str, hex_to_rgb(hex_color), scale)


def DrawFrameNumber():
	text = font.render("Frame "+str(frame), True, (90, 90, 90))
	text_rect = text.get_rect(center=(SCREEN_WIDTH/2, 15))
	screen.blit(text, text_rect)


biggest_timestamp = 0
biggest_index = 0
last_index = -1
def AnimateElements(textelements, timestamp):
	global BG_COLOR_ID
	global biggest_timestamp
	global biggest_index
	global last_index
	for i in range(0, len(textelements)-1):
		if textelements[i+1].timestamp >= timestamp:
			biggest_timestamp = textelements[i+1].timestamp
			biggest_index = i
			break
	text_scale = 0.75
	t = min(1.0, (timestamp - textelements[biggest_index].timestamp) * 6)
	#t = min(1.0, (timestamp - textelements[biggest_index].timestamp))
	if timestamp != 0:
		#text_scale = easeinout(0.75, 1.0, t)
		#text_scale = easeOutExpo(0.75, 1.0, t)
		text_scale = easeOutBack(t)
	DrawCenterText_tuple(textelements[biggest_index].text, cur_text_color, text_scale)
	if last_index != biggest_index:
		last_index = biggest_index
		BG_COLOR_ID = random.randint(0,len(COLORS)-1)

def update_colors():
	global BG_COLOR_ID
	global TEXT_COLOR_ID
	global cur_bg_color
	global cur_text_color
	global last_index
	global biggest_index

	if last_index != biggest_index:
		last_index = biggest_index
		BG_COLOR_ID = random.randint(0,len(COLORS)-1)
		TEXT_COLOR_ID = random.randint(0,len(COLORS_TEXT)-1)

	desired_bg_colors = list(COLORS[BG_COLOR_ID])
	desired_text_colors = list(COLORS_TEXT[TEXT_COLOR_ID])

	# background
	cur_bg_colors = list(cur_bg_color)
	cur_bg_colors[0] = easeinout(cur_bg_colors[0], desired_bg_colors[0], 1.0/FPS * 8)
	cur_bg_colors[1] = easeinout(cur_bg_colors[1], desired_bg_colors[1], 1.0/FPS * 8)
	cur_bg_colors[2] = easeinout(cur_bg_colors[2], desired_bg_colors[2], 1.0/FPS * 8)
	cur_bg_color = tuple(cur_bg_colors)
	
	# text
	#cur_text_colors = list(cur_text_color)
	#cur_text_colors[0] = easeinout(cur_text_colors[0], desired_text_colors[0], 1.0/FPS * 8)
	#cur_text_colors[1] = easeinout(cur_text_colors[1], desired_text_colors[1], 1.0/FPS * 8)
	#cur_text_colors[2] = easeinout(cur_text_colors[2], desired_text_colors[2], 1.0/FPS * 8)
	#cur_text_color = tuple(cur_text_colors)
	cur_text_color = COLORS_TEXT[TEXT_COLOR_ID]
	

# Game Loop
running = True
realtime_playback = False
if realtime_playback:
	pygame.mixer.init()
	pygame.mixer.music.load("scripts/audio.wav")
	pygame.mixer.music.play()

while running:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False

	for i in range(0, len(TextElements)-1):
		if TextElements[i+1].timestamp >= cur_timestamp:
			biggest_timestamp = TextElements[i+1].timestamp
			biggest_index = i
			break
	update_colors()

	screen.fill(cur_bg_color)
	#DrawFrameNumber()
	AnimateElements(TextElements, cur_timestamp)
	pygame.display.update()
	if realtime_playback:
		frame += 1.0
		cur_timestamp = cur_timestamp + 1.0/FPS
		time.sleep(1.0/FPS)
	else:
		SaveFrame()
		frame += 1
		cur_timestamp = cur_timestamp + 1.0/FPS
	if cur_timestamp >= max_timestamp:
		running = False

pygame.quit()
